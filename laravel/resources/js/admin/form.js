$(document).ready(function () {
    let $select2 = $('.select2')
    let $editor = $('.editor')
    let $imgPicker = $('.img-picker')

    function readURL(input) {
        let $imgPreview = $(this).siblings('.img-preview')
        // console.log($imgPreview.attr('src', 'http://localhost:8000/images/bumiyasa-logo-colored.svg'))

        // return

        let target = $(input.target).data('target')
        if (input.target.files && input.target.files[0]) {
            var reader = new FileReader()

            reader.onload = function (e) {
                // $(this).siblings('.img-preview')[0].attr('src', e.target.result)
                $imgPreview.attr('src', e.target.result)
            }

            reader.readAsDataURL(input.target.files[0])
        }
    }

    $imgPicker.change(readURL)

    $editor.each(function () {
        new Jodit(this);

        let isInvalid = $(this).hasClass('is-invalid')

        if (isInvalid) {
            let $container = $(this).parent().find('.jodit_container')[0]

            $($container).addClass('border border-danger')
        }
    })

    $select2.select2()

    $select2.each(function (i, obj) {
        let isInvalid = $(obj).hasClass('is-invalid')

        if (isInvalid) {
            let $selectContainer = $(obj).parent().find('span.select2-container')[0]
            $($selectContainer).addClass('border rounded border-danger')
        }
    });

    flatpickr('.datepicker')

})

window.$ = window.jQuery = require('jquery');
window._ = require('lodash');
window.Panel = {
    Dt: {},
    Dialog: {}
}


require('./dialog')
require('./form')
require('bootstrap/dist/js/bootstrap.bundle.min')
require('../../../vendor/almasaeed2010/adminlte/dist/js/adminlte.min');
require('select2')
require('datatables.net-bs4');
require('flatpickr');
require('sweetalert');
let jodit = require('jodit')


window.Jodit = jodit.Jodit;
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

$(document).ready(function() {

    let dtDefaultColumn = [{
        data: 'DT_RowIndex',
        name: 'DT_RowIndex',
        orderable: false,
        searchable: false
    }]

    var $table = $('.dt').DataTable({
        processing: true,
        serverSide: true,
        order: [
            [1, "desc"]
        ],
        ajax: Panel.Dt.path,
        columns: dtDefaultColumn.concat(Panel.Dt.columns)
    });

    $table.on('draw', function() {
        $('.btn-delete').click(function(e) {
            e.preventDefault()
            Panel.Dialog.confirm($(e.target)[0].href)
        })
    })



})

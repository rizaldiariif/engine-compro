$(function () {
    Panel.Dialog = {
        confirm: async function (url, type = 'delete') {
            console.log(url)
            let $form = $('#form-delete')

            let defaultOpt = {
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                buttons: true,
            }

            if (type == 'delete') {
                Object.assign(defaultOpt, {
                    icon: "warning",
                    dangerMode: true
                })
            }

            let isConfirm = await swal(defaultOpt)
            if (isConfirm) {
                $form.attr('action', url).submit()
            }
        }
    }
})

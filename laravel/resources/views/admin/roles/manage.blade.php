@extends('admin.master')

@section('content')
<div class="d-flex justify-content-between align-items-center px-2">
    <div class="d-flex flex-column">
        @php( $title = ($formConfig['method'] == 'post') ? 'Create New' : 'Edit' )
        <h1>{{ $title }} Role</h1>
    </div>
</div>
@php($permClass = $errors->has('permissions') ? 'border-danger' : '')
<div class="card">
    <div class="card-body">
        {{ Form::open(['url' => $formConfig['path'], 'method' => $formConfig['method'] ]) }}
        <div class="form-group row">
            {{ Form::label('name', 'Name', ['class' => 'col-sm-2 col-form-label']) }}
            <div class="col-sm-10">
                {{
                    Form::text(
                        'name',
                        old('name', isset($role) ? $role->name : null),
                        ['class' => Helper::inputErrorClass("name", $errors)]
                    )
                }}
                @if ($errors->has('name'))
                {{ Form::label(null, $errors->first('name'), ['class' => 'text-danger']) }}
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Permissions</label>
            <div class="col-sm-10">
                <div class="border rounded {{ $permClass }}">
                    @foreach($permissions as $key => $item)
                    <div class="d-flex p-3 border-top">
                        <div class="col-md-4">
                            <div class="py-2">
                                <h3 class="text-primary mb-0">{{ Helper::humanize($key) }}</h3>
                            </div>
                        </div>
                        <div class="col-md-8 row align-items-center">
                            @foreach($item as $perm)
                            <div class="col-sm-4">
                                <div class="form-check">
                                    @if ($formConfig['method'] == 'put')
                                    @php($isChecked = in_array($perm['name'], $rolePermissions) ? 'checked' : '')
                                    @else
                                    @php($isChecked = '')
                                    @endif
                                    <input class="form-check-input" type="checkbox" value="{{ $perm['id'] }}" id="{{ $perm['name'] }}" name="permissions[]" {{ $isChecked }}>
                                    <label class="form-check-label" for="{{ $perm['name'] }}">
                                        @php($action = explode('.', $perm['name']))
                                        {{ Helper::humanize($action[1]) }} {{ Helper::humanize($action[0]) }}
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
                {{ Form::label(null, $errors->first('permissions'), ['class' => 'text-danger']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
                {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

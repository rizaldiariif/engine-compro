@extends('adminlte::page')

@section('content_header')
@if(Session::has('message'))
<div class="alert alert-{{ Session::get('message_type') }} alert-dismissible">
    <button type="button" class="close">×</button>
    <p class="mb-0">{{ Session::get('message') }}</p>
    @if($errors->messages())
    <ul class="mb-0">
        @foreach ($errors->messages() as $item)
            <li>{{ $item[0] }}</li>
        @endforeach
    </ul>
    @endif
</div>
@endif
{!! Form::open(['method' => 'delete', 'id' => 'form-delete']) !!}
{!! Form::close() !!}
@stop


@section('js')
<script>
    @if(isset($dt))
    Panel.Dt.path = "{{ $dt['path'] }}"
    Panel.Dt.columns = {!! json_encode($dt['columns']) !!}
    @endif
</script>
@endsection

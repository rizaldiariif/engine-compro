@extends('admin.master')

@section('content')
<div class="d-flex justify-content-between align-items-center px-2">
    <div class="d-flex flex-column">
        @php( $title = ($formConfig['method'] == 'post') ? 'Create New' : 'Edit' )
        <h1>{{ $title }} </h1>
    </div>
</div>
<div class="card">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs">
            @if ($formConfig['fields'])
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#master-form-tab">Master</a>
            </li>
            @endif
            @if ($formConfig['translatable'])
            @foreach (Config::get('translatable.locales'); as $item)
            @php ($active = (!$formConfig['fields'] && $loop->first) ? 'active' : '')
            <li class="nav-item">
                <a class="nav-link {{ $active }}" data-toggle="pill" href="#{{ $item }}-form-tab">{{ Str::upper($item) }}</a>
            </li>
            @endforeach
            @endif
        </ul>
    </div>
    <div class="card-body">
        {{ Form::open(['url' => $formConfig['path'], 'method' => $formConfig['method'], 'files' => true ]) }}
        <div class="tab-content" id="locale-tab">
            @if ($formConfig['fields'])
            <div class="tab-pane fade show active" id="master-form-tab">
                @include('admin.bread.form', ['locale' => false])
            </div>
            @endif
            @if ($formConfig['translatable'])
            @foreach (Config::get('translatable.locales'); as $item)
            @php ($active = (!$formConfig['fields'] && $loop->first) ? 'active' : '')
            <div class="tab-pane fade show {{ $active }}" id="{{ $item }}-form-tab">
                @include('admin.bread.form', ['locale' => $item])
            </div>
            @endforeach
            @endif
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

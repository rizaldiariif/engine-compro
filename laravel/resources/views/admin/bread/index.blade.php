@extends('admin.master')

@section('content')
<div class="d-flex justify-content-between align-items-center p-2">
    <div class="d-flex flex-column">
        <h1>{{ preg_replace('/[A-Z]/', ' $0', $content['model']) }}</h1>
    </div>
    <div>
        <a
            href="{{ route('admin.'.$content['routes'].'.create') }}"
            class="btn btn-primary mr-1">
                New {{ preg_replace('/[A-Z]/', ' $0', $content['model']) }}
        </a>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <table class="table table-bordered table-striped dt w-100">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    @foreach($dt['columns'] as $item)
                    <th>{{ ucfirst($item['name']) }}</th>
                    @endforeach
                </tr>
            </thead>
        </table>
    </div>
</div>
@stop

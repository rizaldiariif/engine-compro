@php
$renderField = (!$locale) ? $formConfig['fields'] : $formConfig['localeFields'];
@endphp

@foreach ($renderField as $field)
<div class="form-group row">
    {{ Form::label(
            $field['name'],
            isset($field['label']) ? $field['label'] : ucwords($field['name']),
            ['class' => 'col-sm-2 col-form-label']
        )
    }}
    <div class="col-sm-10">
        @if ($field['type'] == 'text')
        {{
            Form::text(
                Helper::getInputName($field, $locale),
                old(
                    Helper::getInputName($field, $locale),
                    Helper::getInputVal($locale, $formData, $field)
                ),
                [
                    'class' => Helper::inputErrorClass(
                        Helper::getInputName($field, $locale),
                        $errors
                    )
                ]
            )
        }}
        @elseif ($field['type'] == 'multiple')
        {{
            Form::select(
                $field['name'] . '[]',
                $field['options'],
                old($field['name'], isset($formData) ? $formData[$field['name']] : null),
                [
                    'class' => 'select2 '. Helper::inputErrorClass($field['name'], $errors),
                    'multiple' => 'multiple'
                ]
            )
        }}
        @elseif ($field['type'] == 'select')
        {{
            Form::select(
                $field['name'],
                $field['options'],
                old($field['name'], isset($formData) ? $formData[$field['name']] : null),
                array_merge(
                    [
                        'class' => (array_key_exists('class', $field) ? $field['class'] : '') . ' ' . Helper::inputErrorClass($field['name'], $errors)
                    ],
                    array_key_exists('attr', $field) ? $field['attr'] : []
                )
            )
        }}
        @elseif ($field['type'] == 'date')
        {{
            Form::text(
                $field['name'],
                old($field['name'], isset($formData) ? $formData[$field['name']] : null),
                ['class' => 'datepicker ' . Helper::inputErrorClass($field['name'], $errors)]
            )
        }}
        @elseif ($field['type'] == 'time')
        {{
            Form::time(
                Helper::getInputName($field, $locale),
                old(
                    Helper::getInputName($field, $locale),
                    Helper::getInputVal($locale, $formData, $field)
                ),
                [
                    'class' => Helper::inputErrorClass(
                        Helper::getInputName($field, $locale),
                        $errors
                    )
                ]
            )
        }}
        @elseif ($field['type'] == 'wysiwyg')
        {{
            Form::textarea(
                Helper::getInputName($field, $locale),
                old(Helper::getInputName($field, $locale), isset($formData) ? $formData[Helper::getInputName($field, $locale)] : null),
                ['class' => 'editor ' . Helper::inputErrorClass(Helper::getInputName($field, $locale), $errors)]
            )
        }}
        @elseif ($field['type'] == 'file')
        {{
            Form::file(
                Helper::getInputName($field, $locale),
                [
                    'class' => 'h-auto img-picker ' . Helper::inputErrorClass(Helper::getInputName($field, $locale), $errors),
                    'data-target' => 'img-preview'
                ]
            )
        }}
        @php ($imgClass = ($formConfig['method'] == 'put' && $formData[$field['name']]) ? 'border' : '' )
        <img src="{{ Helper::getInputVal($locale, $formData, $field) }}"
            class="mt-3 img-fluid {{ $imgClass }} img-preview" />
        @endif
        @if ($errors->has(Helper::getInputName($field, $locale)))
        {{ Form::label(null, $errors->first(Helper::getInputName($field, $locale)), ['class' => 'text-danger']) }}
        @endif
    </div>
</div>
@endforeach
<div class="form-group row">
    <div class="offset-sm-2 col-sm-10">
        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    </div>
</div>

@foreach ($content as $item)
    <div class="form-group row">
        {{
            Form::label(
                $item->label,
                $item->label,
                ['class' => 'col-sm-2 col-form-label']
            )
        }}
        <div class="col-sm-10">
            @if ($item->type == 'text')
            {{
                Form::text(
                    $item->key . ':' . $locale,
                    old($item->key . ':' . $locale, $item->translate($locale)->value),
                    ['class' => Helper::inputErrorClass($item->key . ':' . $locale, $errors)]
                )
            }}
            @elseif ($item->type == 'wysiwyg')
            {{
                Form::textarea(
                    $item->key . ':' . $locale,
                    old($item->key . ':' . $locale, $item->translate($locale)->value),
                    ['class' => 'editor ' . Helper::inputErrorClass($item->key . ':' . $locale, $errors)]
                )
            }}
            @elseif ($item->type == 'multiple_text')
            {{
                Form::select(
                    $item->key . '[]',
                    $options[$item->key],
                    old($item->key, explode(':', $item->value)),
                    [
                        'class' => 'multi-text '. Helper::inputErrorClass($item->key, $errors),
                        'multiple' => 'multiple'
                    ]
                )
            }}
            @elseif ($item->type == 'file')
            {{
                Form::file(
                    $item->key . ':' . $locale,
                    [
                        'class' => 'h-auto img-picker ' . Helper::inputErrorClass($item->key, $errors),
                        'data-target' => 'img-preview'
                    ]
                )
            }}
            @if (!empty($item->hint))
                <small>{{ $item->hint }}</small>
            @endif
            <img src="{{ $item->translate($locale)->value }}" class="mt-3 border img-preview" />
            @endif

            @if ($errors->has($item->key . ':' . $locale))
            {{ Form::label(null, $errors->first($item->key . ':' . $locale), ['class' => 'text-danger']) }}
            @endif
        </div>
    </div>
@endforeach
<div class="form-group row">
    <div class="offset-sm-2 col-sm-10">
        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    </div>
</div>

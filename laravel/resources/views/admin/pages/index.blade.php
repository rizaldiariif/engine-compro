@extends('admin.master')

@section('content')
<div class="d-flex justify-content-between align-items-center p-2">
    <div class="d-flex flex-column">
        <h1>{{ ucwords($section) }}</h1>
    </div>
</div>
<div class="card">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" id="locale-tab" role="tablist">
            @foreach (Config::get('translatable.locales'); as $item)
            <li class="nav-item">
                @if ($loop->first)
                <a class="nav-link active" data-toggle="pill" href="#{{ $item }}-form-tab">{{ Str::upper($item) }}</a>
                @else
                <a class="nav-link" data-toggle="pill" href="#{{ $item }}-form-tab">{{ Str::upper($item) }}</a>
                @endif
            </li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        {{ Form::open(['url' => route('admin.pages.update', ['page' => $section]), 'method' => 'PUT', 'files' => true ]) }}
        <div class="tab-content" id="locale-tab">
            @foreach (Config::get('translatable.locales'); as $item)
                @if ($loop->first)
                <div class="tab-pane fade show active" id="{{ $item }}-form-tab">
                    @include('admin.pages.form', ['locale' => $item])
                </div>
                @else
                <div class="tab-pane fade show" id="{{ $item }}-form-tab">
                    @include('admin.pages.form', ['locale' => $item])
                </div>
                @endif
            @endforeach
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop

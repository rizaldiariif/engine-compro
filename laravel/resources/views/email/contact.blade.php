<div>
    From: [{{ $name }} - {{ $email }}]
</div>
<br><br>
<div>
    Message Body:<br>
    {{ $bodyMessage }}
</div>
<br><br>

<span>--</span>
<div>
    This email was sent from a contact form on EVOS Company Profile Website (https://www.evos.com)
</div>

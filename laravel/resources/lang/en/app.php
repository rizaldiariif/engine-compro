<?php

return [
    'contact_success_message' => 'We have received your message and would like to thank you for writing to us.',
    'career_success_message' => 'Thanks, Your submission has been received!',
    'newsletter_success_message' => 'Thanks, Newsletter subscription has been received!',
    'comment_success_message' => 'Thanks, Your comment is submitted!',
];

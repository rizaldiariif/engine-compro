<?php

return [
    'contact_success_message' => 'Kami telah menerima pesan Anda dan ingin mengucapkan terima kasih karena telah menulis kepada kami.',
    'career_success_message' => 'Terima kasih, kiriman Anda telah diterima!',
    'newsletter_success_message' => 'Terima kasih, Pendaftaran newsletter Anda telah diterima!',
    'comment_success_message' => 'Terima kasih, Komentar Anda telah diterima!',
];

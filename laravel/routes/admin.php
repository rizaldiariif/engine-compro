<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes([
    'register' => false,
    'confirm' => false,
    'reset' => false
]);

Route::group(['middleware' => ['auth'], 'namespace' => 'Admin'], function () {
    Route::get('/', 'DashboardController@index')->name('home');

    // Pages
    Route::get('pages/{page}', 'PageController@index')->name('pages.index');
    Route::put('pages/{page}', 'PageController@update')->name('pages.update');

    // Modules
    Route::resource('articles', 'ArticleController');
    Route::resource('article_categories', 'ArticleCategoryController');

    // System
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');

    if (config('app.env', 'local') == 'local') Route::resource('permissions', 'PermissionController');
});

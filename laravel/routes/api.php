<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api'], function () {
    Route::get('/', 'ApiController@index');
    Route::get('/news', 'ApiController@news');
    Route::get('/news_categories', 'ApiController@news_categories');

    Route::get('/get_dynamic_routes', 'ApiController@get_dynamic_routes');
    Route::get('/contents', 'ApiController@contents');
});

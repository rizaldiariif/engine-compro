# Sitemap

- Home
- About Us
- Projects
- Product
- Services
- News
- Careers
- Sustainability
- Contact

Content

Contact
- FB Link Account
- IG Link Account
- Linkedin Link Account
- Recipient's Email for contact form
- Phone & Fax Number
- Contact Email
- Address

About Us
- Vision & Mission
- What we do
- Management Team (Profile Picture, Name, Title)

Projects
- Existing Project

Product
- Product List Content

Services
- Services Content

Careers
- Recipient's Email for Career Form
- Available Position + Requirement

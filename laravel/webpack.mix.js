const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/admin/bootstrap.js', 'public/js/admin.min.js')
// .sass('resources/sass/admin/app.scss', 'public/css/admin.min.css');
mix.setPublicPath('../public_html');

mix.js('resources/js/site/app.js', 'js/app.js')
    .sass('resources/sass/site/app.scss', 'css/app.css')
    .copyDirectory('resources/fonts', 'fonts')
    .version();

mix.js('resources/js/admin/bootstrap.js', 'js/admin.min.js')
    .sass('resources/sass/admin/app.scss', 'css/admin.min.css')
    .version();

mix.browserSync('127.0.0.1');

<?php

namespace App\Providers;

use App\Models\Content;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);

        if (app()->environment(['remote', 'stag', 'staging', 'prod', 'production'])) {
            URL::forceScheme('https');
        }

        if (!$this->app->runningInConsole()) {
            $content = Content::all();
            $pages = $content->groupBy('section')->toArray();

            $events->listen(BuildingMenu::class, function (BuildingMenu $event) use ($pages) {
                foreach ($pages as $key => $value) {
                    $event->menu->addAfter('page', [
                        'text' => ucwords($key),
                        'url'  => 'admin/pages/' . $key,
                        'can' => 'pages.' . $key
                    ]);
                }
            });
        }

        $this->app->bind('path.public', function () {
            return base_path() . '/../public_html';
        });
    }
}

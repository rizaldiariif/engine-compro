<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function getAvatarAttribute($val)
    {
        if ($val) {
            return asset('img/' . $val);
        } else {
            return 'https://ui-avatars.com/api/?size=160&name=' . $this->name;
        }
    }

    public function adminlte_image()
    {
        return 'https://ui-avatars.com/api/?size=160&name=' . $this->name;
    }

    public function adminlte_desc()
    {
        return $this->getRoleNames()[0];
    }

}

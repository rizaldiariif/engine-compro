<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class CheckLocale
{
    public function handle($request, Closure $next)
    {
        if (Session::has('locale') AND in_array(Session::get('locale'), config('translatable.locales'))) {
            App::setLocale(Session::get('locale'));
        }
        else {
            App::setLocale(config('translatable.locale'));
        }
        return $next($request);
    }
}

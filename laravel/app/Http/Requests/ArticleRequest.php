<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $items = [
            'title' => 'required',
            'content' => 'required',
            'slug' => 'required',
        ];

        $locales = config('translatable.locales');

        $rules = [];
        foreach ($items as $key => $value) {
            foreach ($locales as $locale) {
                $rules[$key . ':' . $locale] = $value;
            }
        }

        $rules['article_category_id'] = 'required';
        $rules['user_id'] = 'required';
        $rules['date'] = 'required';
        $rules['thumbnail'] = 'sometimes|required';

        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = isset($this->user) ? $this->user : '';
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'password' => 'sometimes|required|min:8',
            'roles' => 'required',
            'avatar' => 'required|max:2048'
        ];

        if ($this->route('user')) $rules['avatar'] = 'sometimes|required|max:2048';

        return $rules;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\App;

class ContentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returned_data = [];
        $collection = $this->collection;
        foreach ($collection as $key => $value) {
            $returned_data[$value['key']] = $value->translate(App::getLocale());
        }
        return $returned_data;
    }
}

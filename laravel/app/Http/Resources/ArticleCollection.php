<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\App;

class ArticleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection;
        $returned_data = [];
        foreach ($collection as $key => $value) {
            $returned_data[$key] = array_merge($value->toArray(), $value->translate(App::getLocale())->toArray());
            unset($returned_data[$key]['translations']);
        }
        return [
            'data' => $returned_data
        ];
    }
}

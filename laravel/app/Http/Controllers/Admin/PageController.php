<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    protected $content;

    public function __construct()
    {
        $section = request()->segment(3);
        $content = Content::whereSection($section)->get();
        $this->content = $content;
    }

    public function index(Request $request, $section)
    {
        if (!Gate::allows('pages.' . $section)) {
            return abort(401);
        }

        $content = $this->content;

        $multi = array_filter($this->content->toArray(), function ($item) {
            return ($item['type'] == 'multiple_text');
        });

        $options = [];

        foreach ($multi as $item) {
            $data = explode(':', $item['value']);
            $options[$item['key']] = [];
            foreach ($data as $idx => $value) {
                $options[$item['key']][$value] = $value;
            }
        }

        return view('admin.pages.index', compact('content', 'section', 'options'));
    }

    public function update(Request $request, $section)
    {
        $rules = [];
        $data = $request->except(['_method', '_token']);

        $locales = config('translatable.locales');

        foreach ($this->content as $item) {
            foreach ($locales as $lang) {
                $rules[$item->key . ':' . $lang] = $item->validation;
            }
        }

        try {
            $validator = Validator::make($data, $rules);
        } catch (\Throwable $th) {
            dd($th);
        }

        if ($validator->fails()) {
            return redirect()
                ->route('admin.pages.index', ['page' => $section])
                ->withErrors($validator->messages())
                ->withMessage('There was an error with your submission')
                ->withMessageType('danger')
                ->withInput();
        }

        $files = array_filter($this->content->toArray(), function ($item) {
            return ($item['type'] == 'file');
        });

        $multiText = array_filter($this->content->toArray(), function ($item) {
            return ($item['type'] == 'multiple_text');
        });

        foreach ($multiText as $field) {
            $data[$field['key']] = join(':', $request[$field['key']]);
        }

        foreach ($files as $field) {
            try {
                foreach ($locales as $key => $value) {
                    if ($request->hasFile($field['key'] . ':' . $value)) {

                        $file = $request->file($field['key'] . ':' . $value)->store('pages');
                        $data[$field['key'] . ':' . $value] = asset('images/' . $file);
                    }
                }
            } catch (\Throwable $th) {
                dd($th);
            }
        }

        $formData = [];

        foreach ($locales as $value) {
            $formData[$value] = [];
        }

        foreach ($data as $key => $value) {
            $locale = explode(':', $key);
            $formData[$locale[1]][$locale[0]] = $value;
        }

        try {
            foreach ($this->content as $i => $row) {

                $contentData = [];
                foreach ($locales as $key => $value) {
                    if (key_exists($row->key, $formData[$value])) {
                        $contentData['value:' . $value] = $formData[$value][$row->key];
                    }
                }

                $row->update($contentData);
            }
        } catch (\Throwable $th) {
            dd($th);
        }

        return redirect()
            ->route('admin.pages.index', ['page' => $section])
            ->withMessage(ucwords($section) . ' Page has been successfully created')
            ->withMessageType('success');
    }
}

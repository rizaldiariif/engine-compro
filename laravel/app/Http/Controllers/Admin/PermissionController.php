<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Traits\BREAD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;

class PermissionController extends Controller
{
    use BREAD;

    protected $translatable = false;
    protected $modelClass = 'Spatie\Permission\Models\Permission';
    protected $gate = 'permission';
    protected $route = 'permissions';
    protected $model = 'Permission';
    protected $formRequest = 'PermissionRequest';
    protected $columns = [
        array('data' => 'name', 'name' => 'name'),
        array('data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false)
    ];
    protected $fields = [
        array('name' => 'name', 'type' => 'text'),
    ];
}

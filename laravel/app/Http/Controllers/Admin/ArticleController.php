<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use App\Models\ProjectCategory;
use App\Traits\BREAD;
use App\User;
use Illuminate\Support\Facades\Route;

class ArticleController extends Controller
{
    use BREAD;

    protected $customValidation = true;
    protected $translatable = true;
    protected $gate = 'article';
    protected $route = 'articles';
    protected $modelClass = 'App\Models\Article';
    protected $model = 'Article';
    protected $formRequest = 'ArticleRequest';
    protected $columns = [
        array('data' => 'title', 'name' => 'title'),
        array('data' => 'thumbnail', 'name' => 'thumbnail', 'type' => 'image'),
        array('data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false)
    ];
    protected $fields = [
        array(
            'label' => 'Category',
            'name' => 'article_category_id',
            'type' => 'select',
            'options' => []
        ),
        array(
            'label' => 'Author',
            'name' => 'user_id',
            'type' => 'select',
            'options' => []
        ),
        array('name' => 'date', 'type' => 'date'),
        array('name' => 'thumbnail', 'type' => 'file', 'dest' => 'articles'),
    ];

    protected $localeFields = [
        array('name' => 'title', 'type' => 'text'),
        array('name' => 'content', 'type' => 'wysiwyg'),
        array('name' => 'slug', 'type' => 'text')
    ];

    public function __construct()
    {
        if (Route::currentRouteName() != 'admin.articles.index') {
            $this->fields[0]['options'] = ArticleCategory::get()->pluck('name', 'id');
            $this->fields[1]['options'] = User::get()->pluck('name', 'id');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\BREAD;

class ArticleCategoryController extends Controller
{
    use BREAD;

    protected $translatable = false;
    protected $gate = 'article_category';
    protected $route = 'article_categories';
    protected $modelClass = 'App\Models\ArticleCategory';
    protected $model = 'ArticleCategory';
    protected $formRequest = 'ArticleCategoryRequest';
    protected $columns = [
        array('data' => 'name', 'name' => 'name'),
        array('data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false)
    ];
    protected $fields = [
        array('name' => 'name', 'type' => 'text'),
    ];
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\BREAD;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    use BREAD;

    protected $translatable = false;
    protected $gate = 'user';
    protected $route = 'users';
    protected $modelClass = 'App\User';
    protected $model = 'User';
    protected $formRequest = 'UserRequest';
    protected $columns = [
        array('data' => 'name', 'name' => 'name'),
        array('data' => 'email', 'name' => 'email'),
        array('data' => 'avatar', 'name' => 'avatar', 'type' => 'image'),
        array('data' => 'roles', 'name' => 'roles', 'type' => 'custom'),
        array('data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false)
    ];
    protected $fields = [
        array('name' => 'name', 'type' => 'text'),
        array('name' => 'email', 'type' => 'text'),
        array('name' => 'password', 'type' => 'text'),
        array(
            'name' => 'roles',
            'type' => 'multiple',
            'options' => []
        ),
        array('name' => 'avatar', 'type' => 'file', 'dest' => 'users/avatar'),
    ];

    public function __construct()
    {
        if (Route::currentRouteName() == 'admin.users.show') {
            unset($this->fields[2]);
        }
        $this->fields[3]['options'] = Role::get()->pluck('name', 'name');
    }

    public function beforeShow($formData)
    {
        // $data = $formData;
        $formData->roles = $formData->roles->pluck('name')->toArray();
        return $formData;
    }

    public function getCustomColumn($column, $row)
    {
        $col = '<div>';
        $roles = $row->roles->pluck('name')->toArray();

        foreach ($roles as $val) {
            $col .= '<span class="badge badge-primary ml-1">' . $val . '</span>';
        }

        $col .= '</div>';
        return $col;
    }

    public function afterCreate($model, $request)
    {
        $model->assignRole($request->input('roles'));
    }

    public function afterUpdate($model, $request)
    {
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $model->syncRoles($roles);
    }
}

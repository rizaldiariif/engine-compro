<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Traits\BREAD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{

    use BREAD;

    protected $gate = 'role';
    protected $route = 'roles';
    protected $modelClass = 'Spatie\Permission\Models\Role';
    protected $model = 'Role';
    protected $formRequest = 'RoleRequest';
    protected $columns = [
        array('data' => 'name', 'name' => 'name'),
        array('data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false)
    ];

    public function getAllPermission()
    {
        $permissionName = Permission::select('name', 'id')->get()->toArray();
        $permissions = [];

        foreach ($permissionName as $item) {
            $title = ucfirst(explode('.', $item['name'])[0]);

            if (array_key_exists($title, $permissions)) array_push($permissions[$title], $item);
            else {
                $permissions[$title] = [$item];
            }
        }

        return $permissions;
    }

    public function create()
    {
        if (!Gate::allows('role.add')) {
            return abort(401);
        }

        $permissions = $this->getAllPermission();

        $formConfig = array('path' => route('admin.roles.store'), 'method' => 'post');

        return view('admin.roles.manage', compact('formConfig', 'permissions'));
    }

    public function store(RoleRequest $request)
    {
        $role = Role::create(['name' => $request->name]);
        $role->permissions()->sync($request->permissions);

        return redirect()->route('admin.roles.index')
            ->withMessage('Role ' . $role->name . ' created')
            ->withMessageType('success');
    }

    public function show($id)
    {
        if (!Gate::allows('role.read')) {
            return abort(401);
        }

        $role = Role::findOrFail($id);
        $rolePermissions = $role->permissions->pluck('name')->toArray();

        $permissions = $this->getAllPermission();
        $formConfig = array('path' => route('admin.roles.update', ['role' => $id]), 'method' => 'put');

        return view('admin.roles.manage', compact('formConfig', 'role', 'permissions', 'rolePermissions'));
    }

    public function edit($id)
    {
        //
    }

    public function update(RoleRequest $request, Role $role)
    {
        if (!Gate::allows('role.edit')) {
            return abort(401);
        }

        $role->update(['name' => $request->name]);
        $role->syncPermissions($request->permissions);

        return redirect()->route('admin.roles.index')
            ->withMessage('Role ' . $role->name . ' updated')
            ->withMessageType('success');;
    }

    public function destroy(Role $role)
    {
        if (!Gate::allows('user.delete')) {
            return abort(401);
        }

        $role->delete();
        return redirect()->route('admin.roles.index')
            ->withMessage('Role ' . $role->name . ' deleted')
            ->withMessageType('success');
    }
}

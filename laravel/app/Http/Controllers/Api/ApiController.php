<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCategoryCollection;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ContentCollection;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function index()
    {
        return "Hello World";
    }

    public function news(Request $request)
    {
        $params = $request->query();

        $results = $this->advancedResult(new Article, $params);

        return new ArticleCollection($results);
    }

    public function news_categories(Request $request)
    {
        $params = $request->query();

        $results = $this->advancedResult(new ArticleCategory, $params);

        return new ArticleCategoryCollection($results);
    }

    public function get_dynamic_routes(Request $request)
    {
        $model = $request->query('model');

        $model = 'App\Models\\' . $model;

        return $model::get();
    }

    public function contents(Request $request)
    {
        $params = $request->query();

        if (isset($params['lang'])) {
            App::setLocale($params['lang']);
        } else {
            App::setLocale('en');
        }

        $keys = explode(',', $params['keys']);

        $query = Content::query();

        foreach ($keys as $key => $value) {
            $query->orWhere('key', '=', $value);
        }

        return new ContentCollection($query->get());
    }

    private function advancedResult($model, $params)
    {

        if (isset($params['perPage'])) {
            $perPage = $params['perPage'];
        } else {
            $perPage = 10;
        }

        if (isset($params['lang'])) {
            App::setLocale($params['lang']);
        } else {
            App::setLocale('en');
        }

        unset($params['perPage']);
        unset($params['page']);
        unset($params['lang']);


        $articleModel = $model;
        $translated_attributes = $articleModel->translatedAttributes;

        $query = $model::query();
        foreach ($params as $key => $value) {
            if (in_array($key, $translated_attributes)) {
                $query->whereTranslation($key, $value);
            } else {
                $query->where($key, '=', $value);
            }
        }

        $articles = $query->paginate($perPage);
        $articles->withQueryString();
        return $articles;
    }
}

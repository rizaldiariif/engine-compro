<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class BaseSiteController extends Controller
{
    // Enable for View Scope Shared Content
    // protected $content;

    // public function __construct()
    // {
    //     $this->content = Content::all();
    //     View::share('content', $this->content);
    // }

    public function home()
    {
        return "This is Homepage";
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($category) {
            $category->articles->each(function ($article) {
                $article->delete();
            });
        });
    }
}

<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Model;

class Content extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['value'];

    protected $fillable = [
        'label',
        'key',
        'validation',
        'section',
        'type'
    ];
}

<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\Model;

class Article extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = [
        'title',
        'content',
        'slug'
    ];

    protected $fillable = [
        'article_category_id',
        'user_id',
        'thumbnail',
        'date'
    ];

    public function article_category()
    {
        return $this->belongsTo('App\Models\ArticleCategory');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

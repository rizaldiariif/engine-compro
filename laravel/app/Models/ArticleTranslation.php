<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTranslation extends Model
{
    protected $fillable = [
        'title',
        'content',
        'cta',
        'slug'
    ];
    public $timestamps = false;
}

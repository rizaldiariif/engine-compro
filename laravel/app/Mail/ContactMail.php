<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $this->data['bodyMessage'] = $this->data['comment'];
        return $this->from('contact@evos.com')
            ->view('email.contact')
            ->subject('Contact Form at evos.com')
            ->with($this->data);
    }
}

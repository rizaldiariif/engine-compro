<?php

namespace App\Helpers;

use App\Models\Banner;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Request;

class Helper
{
    public static function inputErrorClass($name, $errors)
    {
        return $errors->has($name) ? 'form-control is-invalid' : 'form-control ';
    }

    public static function humanize($value)
    {
        $value = str_replace("_", " ", $value);
        return ucwords($value);
    }

    public static function wrapHtmlTag($array, $tags = ['p', 'b'])
    {
        $string = '';
        foreach ($array as $value) {
            $i = array_rand($tags);
            $string .= '<' . $tags[$i] . '>' . $value . '</' . $tags[$i] . '>';
        }
        return $string;
    }

    public static function set_active($uri)
    {
        $active = '';
        if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri . '/*') || Request::is($uri)) {
            $active = 'active';
        }
        return $active;
    }

    public static function getInputName($field, $locale)
    {
        return ($locale) ? $field['name'] . ':' . $locale : $field['name'];
    }

    public static function getInputVal($locale, $formData, $field)
    {
        if ($locale) {
            return isset($formData) ? $formData->translate($locale)[$field['name']] : null;
        } else {
            return isset($formData) ? $formData[$field['name']] : null;
        }
    }

    public static function getContent($key, $array)
    {
        $data = $array->filter(function ($value) use ($key) {
            return $value->key == $key;
        });

        $lang = (App::getLocale()) ? App::getLocale() : 'en';

        if ($data->first() == null) dd($key);
        return $data->first()->translate($lang)->value;
    }

    public static function getBanner()
    {
        $route = Route::currentRouteName();
        $banner = Banner::whereType($route)->get();
        $item = [];

        foreach ($banner as $key => $row) {
            $item['title'][$key] = $row->translate(App::getLocale())->title;
            $item['thumbnail'][$key] = $row->thumbnail;
        }

        // dd($item);

        View::share('item', $item);
    }
}

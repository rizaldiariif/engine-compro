<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

trait BREAD
{

    public function index(Request $request)
    {
        if (!Gate::allows($this->gate . '.browse')) {
            return abort(401);
        }

        if (isset($this->modelClass)) {
            $model = $this->modelClass;
        } else {
            $model = 'App\Models\\' . $this->model;
        }

        if ($request->ajax()) {
            $data = $model::latest()->get();
            $rawColumns = ['action'];
            $dt = DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return $this->getActionButton($row);
                });

            foreach ($this->columns as $item) {
                if (isset($item['type']) && $item['type'] == 'image') {
                    $dt = $dt->addColumn($item['name'], function ($row) use ($item) {
                        return '<img class="img-dt" src="' . $row->{$item['name']} . '" />';
                    });

                    array_push($rawColumns, $item['name']);
                } else if (isset($item['type']) && $item['type'] == 'custom') {
                    $dt = $dt->addColumn($item['name'], function ($row) use ($item) {
                        $column = $this->getCommonCustomColumn($item, $row);
                        if (empty($column)) $column = $this->getCustomColumn($item, $row);
                        return $column;
                    });
                    array_push($rawColumns, $item['name']);
                } else if (isset($item['type']) && $item['type'] == 'relation') {
                    $dt = $dt->addColumn($item['name'], function ($row) use ($item) {
                        $column = $this->getRelationColumn($item, $row);
                        return $column;
                    });
                    array_push($rawColumns, $item['name']);
                }
            }

            return $dt->rawColumns($rawColumns)
                ->make(true);
        }

        $dt = array(
            'path' => route('admin.' . $this->route . '.index'),
            'columns' => $this->columns,

        );

        $content = array(
            'model' => $this->model,
            'routes' => $this->route
        );

        return view('admin.bread.index', compact('dt', 'content'));
    }

    public function beforeCreate($request)
    {
    }

    public function create(Request $request)
    {
        if (!Gate::allows($this->gate . '.add')) {
            return abort(401);
        }

        $this->beforeCreate($request);

        $formConfig = array(
            'path' => route('admin.' . $this->route . '.store'),
            'method' => 'post',
            'fields' => isset($this->fields) ? $this->fields : [],
            'localeFields' => isset($this->localeFields) ? $this->localeFields : null,
            'translatable' => $this->translatable
        );

        $formData = null;

        return view('admin.bread.manage', compact('formConfig', 'formData'));
    }

    public function beforeShow($formData)
    {
        return $formData;
    }

    public function show($id)
    {
        if (!Gate::allows($this->gate . '.read')) {
            return abort(401);
        }

        if (isset($this->modelClass)) {
            $model = $this->modelClass;
        } else {
            $model = 'App\Models\\' . $this->model;
        }

        $formData = $model::findOrFail($id);

        $formData = $this->beforeShow($formData);
        $keyId = isset($this->routeParam) ? $this->routeParam : $this->gate;

        $formConfig = array(
            'path' => route('admin.' . $this->route . '.update', [$keyId => $id]),
            'method' => 'put',
            'fields' => isset($this->fields) ? $this->fields : [],
            'localeFields' => isset($this->localeFields) ? $this->localeFields : null,
            'translatable' => $this->translatable,
        );

        return view('admin.bread.manage', compact('formConfig', 'formData'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if (isset($this->customValidation)) {
            $class = '\App\Http\Requests\\' . $this->formRequest;
            $rules = (new $class())->rules();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return redirect()
                    ->route('admin.' . $this->route . '.create')
                    ->withErrors($validator)
                    ->withMessage('There was an error with your submission')
                    ->withMessageType('danger')
                    ->withInput();
            }
        } else {
            App::make('\App\Http\Requests\\' . $this->formRequest);
        }

        $masterFields = isset($this->fields) ? $this->fields : [];

        $allFields = isset($this->localeFields)
            ? array_merge($this->localeFields, $masterFields)
            : $masterFields;

        $files = array_filter($allFields, function ($item) {
            return ($item['type'] == 'file');
        });

        $locales = config('translatable.locales');

        foreach ($files as $field) {
            $dest = $field['dest'];

            foreach ($locales as $key => $value) {
                if ($request->hasFile($field['name'] . ':' . $value)) {
                    $file = $request->file($field['name'] . ':' . $value)->store($dest);
                    $data[$field['name'] . ':' . $value] = asset('images/' . $file);
                } else if ($request->hasFile($field['name'])) {
                    $file = $request->file($field['name'])->store($dest);
                    $data[$field['name']] = asset('images/' . $file);
                }
            }
        }

        if (isset($this->modelClass)) {
            $model = $this->modelClass;
        } else {
            $model = 'App\Models\\' . $this->model;
        }

        $dataModel = $model::create($data);

        if (method_exists($this, 'afterCreate')) {
            $this->afterCreate($dataModel, $request);
        }

        return redirect()->route('admin.' . $this->route . '.index')
            ->withMessage($this->model . ' has been successfully created')
            ->withMessageType('success');
    }

    public function update(Request $request, $id)
    {
        if (!Gate::allows($this->gate . '.edit')) {
            return abort(401);
        }

        $data = $request->all();

        if (isset($this->customValidation)) {
            $class = '\App\Http\Requests\\' . $this->formRequest;
            $rules = (new $class())->rules();

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return redirect()
                    ->route('admin.' . $this->route . '.update', [$this->gate => $id])
                    ->withErrors($validator)
                    ->withMessage('There was an error with your submission')
                    ->withMessageType('danger')
                    ->withInput();
            }
        } else {
            App::make('\App\Http\Requests\\' . $this->formRequest);
            // try {
            // } catch (\Throwable $th) {
            //     dd($th);
            // }
        }

        $masterFields = isset($this->fields) ? $this->fields : [];

        $allFields = isset($this->localeFields)
            ? array_merge($this->localeFields, $masterFields)
            : $masterFields;

        $files = array_filter($allFields, function ($item) {
            return ($item['type'] == 'file');
        });

        $locales = config('translatable.locales');

        foreach ($files as $field) {

            $dest = $field['dest'];

            foreach ($locales as $key => $value) {
                if ($request->hasFile($field['name'] . ':' . $value)) {
                    $file = $request->file($field['name'] . ':' . $value)->store($dest);
                    $data[$field['name'] . ':' . $value] = asset('images/' . $file);
                } else if ($request->hasFile($field['name'])) {
                    $file = $request->file($field['name'])->store($dest);
                    $data[$field['name']] = asset('images/' . $file);
                }
            }
        }

        if (isset($this->modelClass)) {
            $model = $this->modelClass;
        } else {
            $model = 'App\Models\\' . $this->model;
        }

        $model = $model::findOrFail($id);
        $model->update($data);

        if (method_exists($this, 'afterUpdate')) {
            $this->afterUpdate($model, $request);
        }

        return redirect()->route('admin.' . $this->route . '.index')
            ->withMessage($this->model . ' has been successfully updated')
            ->withMessageType('success');
    }

    public function destroy($id)
    {
        if (!Gate::allows($this->gate . '.delete')) {
            return abort(401);
        }

        if (isset($this->modelClass)) {
            $model = $this->modelClass;
        } else {
            $model = 'App\Models\\' . $this->model;
        }

        $model = $model::findOrFail($id);
        $model->delete();

        return redirect()->route('admin.' . $this->route . '.index')
            ->withMessage($this->model . ' has been successfully deleted')
            ->withMessageType('success');
    }

    public function getActionButton($row)
    {
        $keyId = isset($this->routeParam) ? $this->routeParam : $this->gate;
        $btn = '<div>';
        $btn .= '<a href="' . route('admin.' . $this->route . '.show', [$keyId => $row->id]) . '" class="btn btn-secondary btn-sm ml-1">Edit</a>';
        $btn .= '<a href="' . route('admin.' . $this->route . '.destroy', [$keyId => $row->id]) . '" class="btn btn-danger btn-sm ml-1 btn-delete">Delete</a>';
        $btn .= '</div>';
        return $btn;
    }

    public function getCustomColumn($column, $row)
    {
    }

    public function getRelationColumn($column, $row)
    {
        // print_r($row->{$column['method']}->{$column['name']});
        return $row->{$column['method']}->{$column['attribute']};
    }

    public function getCommonCustomColumn($column, $row)
    {
        $priceKey = ['price', 'delivery_fee'];
        $tagKey = ['options', ''];

        if (in_array($column['name'], $priceKey)) {
            return '<p>Rp ' . number_format($row->{$column['name']}, 2, ',', '.') . '</p>';
        } else if (in_array($column['name'], $tagKey)) {
            $words = explode(',', $row->{$column['name']});

            $wrapper = '<div>';
            foreach ($words as $value) {
                $wrapper .= '<label class="badge badge-primary ml-1">' . $value . '</label>';
            }
            $wrapper .= '</div>';
            return $wrapper;
        } else {
            return '';
        }
    }
}

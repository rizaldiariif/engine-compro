<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    public function run()
    {
        $global = [
            [
                'label' => 'Email Hotline',
                'key' => 'email_hotline',
                'value' => 'hello@evos.com',
                'type' => 'text',
                'validation' => 'required|email',
                'section' => 'global'
            ],
            [
                'label' => 'Email Partnership',
                'key' => 'email_partnership',
                'value' => 'proposal@evos.com',
                'type' => 'text',
                'validation' => 'required|email',
                'section' => 'global'
            ],
            [
                'label' => 'Phone Hotline',
                'key' => 'phone_hotline',
                'value' => '+62 8521 9872 93',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Phone Partnership',
                'key' => 'phone_partnership',
                'value' => '+62 8521 9872 91',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Address',
                'key' => 'address',
                'value' => 'Jalan R.A Kartini III-S Kav. 06, RT.6/RW.14, Pd. Pinang Kec. Kby. Lama, Kota Jakarta Selatan, 12310',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Policy Title',
                'key' => 'policy',
                'value' => 'POLICY',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Copyright',
                'key' => 'copyright',
                'value' => 'EVOS eSports © 2020. All Rights Reserved.',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Back CTA',
                'key' => 'back_cta',
                'value' => 'BACK',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Instagram Link',
                'key' => 'ig_link',
                'value' => 'https://www.instagram.com/evosesports/',
                'validation' => 'required|url',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Facebook Link',
                'key' => 'fb_link',
                'value' => 'https://www.facebook.com/teamEVOS/',
                'validation' => 'required|url',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Youtube Link',
                'key' => 'youtube_link',
                'value' => 'https://www.youtube.com/c/EVOSEsports',
                'validation' => 'required|url',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Logo White',
                'key' => 'logo_white',
                'value' => asset('images/pages/logo-evos-bw.png'),
                'type' => 'file',
                'section' => 'global',
                'validation' => 'sometimes|required',
            ],
            [
                'label' => 'Logo Colored',
                'key' => 'logo_colored',
                'value' => asset('images/pages/logo-evos.png'),
                'type' => 'file',
                'section' => 'global',
                'validation' => 'sometimes|required',
            ],
            [
                'label' => 'Contact Email Receiver',
                'key' => 'contact_email_receiver',
                'value' => 'rizaldiarif7@gmail.com',
                'type' => 'text',
                'validation' => 'required|email',
                'section' => 'global'
            ],
            [
                'label' => 'SEO Title',
                'key' => 'seo_title',
                'value' => 'EVOS – EVOS Esports — #1 Esports Team in SEA',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'SEO Description',
                'key' => 'seo_description',
                'value' => 'Join Official Membership and be ready for another big release of EVOS Esports  home match tickets, which will be on sale soon!',
                'type' => 'text',
                'section' => 'global'
            ],
            [
                'label' => 'Favicon',
                'key' => 'favicon',
                'value' => asset('images/pages/favicon.ico'),
                'type' => 'file',
                'section' => 'global',
                'validation' => 'sometimes|required|mimes:ico',
                'hint' => 'Image extension must be .ico'
            ],
        ];

        $data = array_merge(
            $global,
        );

        foreach ($data as $index => $item) {
            if (!array_key_exists('validation', $item)) {
                $data[$index]['validation'] = 'required';
                $data[$index]['hint'] = '';
            }

            $content = Content::create($data[$index]);
            $locales = config('translatable.locales');

            foreach ($locales as $locale) {
                $content->translateOrNew($locale)->value = $data[$index]['value'];
                $content->save();
            }
        }
    }
}

<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        Artisan::call('cache:clear');
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $modules = [
            'user',
            'role',
            'permission',
            'article',
            'article_category',
        ];
        $actions = [];

        foreach ($modules as $value) {
            array_push($actions, ['name' => $value . '.browse']);
            array_push($actions, ['name' => $value . '.read']);
            array_push($actions, ['name' => $value . '.edit']);
            array_push($actions, ['name' => $value . '.add']);
            array_push($actions, ['name' => $value . '.delete']);
        }

        array_push($actions, ['name' => 'panel.access']);
        array_push($actions, ['name' => 'user.change_password']);

        $pages = [
            'global',
        ];

        foreach ($pages as $page) {
            array_push($actions, ['name' => 'pages.' . $page]);
        }

        Permission::insert($actions);

        $superAdmin = Role::create(['name' => 'superadmin']);
        $admin = Role::create(['name' => 'admin']);

        $permissions = Permission::all();

        $superAdmin->permissions()->sync($permissions->pluck('id')->all());
        $admin->givePermissionTo('panel.access');

        $superAdminUser = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'password' => 'superadmin'
        ]);

        $adminUser = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => 'admin'
        ]);

        $superAdminUser->assignRole('superadmin');
        $adminUser->assignRole('admin');
    }
}

<?php

use App\Models\Article;
use App\Models\ArticleCategory;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    public function run()
    {
        $articleCategory = [
            [
                'name' => 'General',
            ],
            [
                'name' => 'Partnership & Sponsorship',
            ],
            [
                'name' => 'EVOS Fams',
            ],
            [
                'name' => 'Mobile Legends',
            ],
            [
                'name' => 'PUBG Mobile',
            ],
            [
                'name' => 'Free Fire',
            ]
        ];

        foreach ($articleCategory as $item) {
            ArticleCategory::create($item);
        }

        $article = [
            [
                'article_category_id' => 1,
                'user_id' => 1,
                'thumbnail' => asset('images/articles/weekly-match-evos-esports-25-31-oktober-2020.jpg'),
                'date' => '2020-10-23',
                'title:en' => 'WEEKLY MATCH EVOS ESPORTS: 25-31 Oktober 2020',
                'title:id' => 'WEEKLY MATCH EVOS ESPORTS: 25-31 Oktober 2020',
                'content:en' => '
                <p>Oktober sudah sampai di minggu yang terakhir. Banyak sekali pertandingan yang sudah dilewati oleh Evos di bulan yang ke sepuluh ini. Di penghujung bulan ini masih tersisa 2 pertandingan lagi untuk 2 divisi di Evos Esports.Siapa sajakah divisi yang bertanding?</p>
                <p><strong>Evos ML Ladies kembali bertanding</strong></p>
                <p>Setelah istirahat selama lebih dari  2 minggu, Evos ML Ladies akan kembali sibuk mengikuti turnamen wanita terbesar di Indonesia yaitu Woman Star League (WSL) Invitational. Pertandingan akan dilaksanakan pada tanggal 25 oktober 2020 jam 16.00 Wib. Funi dan kawan-kawan akan mengeluarkan permainan terbaiknya untuk melawan Belletron Battle Angels.</p>
                <p><strong>Sang juara bermain di grand final IPG 2020</strong></p>
                <p>Jadwal terbaru datang dari juara FFIM 2020 yaitu Evos Free Fire. Setelah memenangkan turnamen bergengsi itu, Evos Free Fire akan kembali menunjukan taringnya di ajang Piala Indonesia Pro Gamers 2020. Evos Free Fire akan bertanding di grand final pertandingan tersebut. Pertandingan ini akan diadakan pada hari terakhir bulan oktober tanggal 31 oktober 2020 jam 20.00 wib.</p>
                <p>Dukung terus Evos Esports untuk mendapatkan yang terbaik. Tidak semua orang bisa konsisten selalu berada di atas, kadang ada saatnya berada di atas dan kadang juga ada saatnya berada di bawah. Terima kasih untuk para Evos Fams yang terus mendukung Evos sampai bisa di titik yang sekarang. Dukung terus Evos divisi favoritmu!</p>
                ',
                'content:id' => '
                <p>Oktober sudah sampai di minggu yang terakhir. Banyak sekali pertandingan yang sudah dilewati oleh Evos di bulan yang ke sepuluh ini. Di penghujung bulan ini masih tersisa 2 pertandingan lagi untuk 2 divisi di Evos Esports.Siapa sajakah divisi yang bertanding?</p>
                <p><strong>Evos ML Ladies kembali bertanding</strong></p>
                <p>Setelah istirahat selama lebih dari  2 minggu, Evos ML Ladies akan kembali sibuk mengikuti turnamen wanita terbesar di Indonesia yaitu Woman Star League (WSL) Invitational. Pertandingan akan dilaksanakan pada tanggal 25 oktober 2020 jam 16.00 Wib. Funi dan kawan-kawan akan mengeluarkan permainan terbaiknya untuk melawan Belletron Battle Angels.</p>
                <p><strong>Sang juara bermain di grand final IPG 2020</strong></p>
                <p>Jadwal terbaru datang dari juara FFIM 2020 yaitu Evos Free Fire. Setelah memenangkan turnamen bergengsi itu, Evos Free Fire akan kembali menunjukan taringnya di ajang Piala Indonesia Pro Gamers 2020. Evos Free Fire akan bertanding di grand final pertandingan tersebut. Pertandingan ini akan diadakan pada hari terakhir bulan oktober tanggal 31 oktober 2020 jam 20.00 wib.</p>
                <p>Dukung terus Evos Esports untuk mendapatkan yang terbaik. Tidak semua orang bisa konsisten selalu berada di atas, kadang ada saatnya berada di atas dan kadang juga ada saatnya berada di bawah. Terima kasih untuk para Evos Fams yang terus mendukung Evos sampai bisa di titik yang sekarang. Dukung terus Evos divisi favoritmu!</p>
                ',
                'slug:en' => 'weekly-match-evos-esports-25-31-oktober-2020-en',
                'slug:id' => 'weekly-match-evos-esports-25-31-oktober-2020-id',
            ],
            [
                'article_category_id' => 6,
                'user_id' => 1,
                'thumbnail' => asset('images/articles/5-karakter-free-fire-yang-cocok-buat-pemula-mudah-dimainkan.jpg'),
                'date' => '2020-10-23',
                'title:en' => '5 Karakter Free Fire yang Cocok Buat Pemula, Mudah Dimainkan!',
                'title:id' => '5 Karakter Free Fire yang Cocok Buat Pemula, Mudah Dimainkan!',
                'content:en' => '
                <p>Free Fire menjadi game yang cukup populer di kalangan gamers saat ini. Game besutan Garena ini sering kali dimainkan oleh para youtuber gaming seperti Dyland Pros, Cepcill, dan juga Jess no Limit. Tidak heran banyak orang yang ingin mencoba memainkan permainan ini.</p>
                <p>Berbeda dengan game battle royale lainnya, Free Fire memiliki karakter dengan kemampuan yang berbeda-beda. Kemampuan yang dimiliki oleh setiap karakter bisa digunakan untuk membantu player yang baru belajar. Kira-kira siapa saja karakter yang cocok untuk digunakan pemula?</p>
                <p><strong>1. Kelly</strong></p>
                <p>Bisa bergerak dengan cepat merupakan kemampuan yang dimiliki oleh karakter ini. Semakin lama Kelly berlari akan semakin besar juga damage yang bisa dikeluarkannya. Kecepatan lari Kelly bisa membantu kalian para player untuk menjauh maupun mendekati musuh dengan mudah. Kalian juga bisa berpindah-pindah dengan cepat sehingga itu bisa membantu kalian menjelajahi map. Kemampuan ini sangat cocok untuk pemula yang masih ingin menghafal map.</p>
                <p><strong>2. Antonio</strong></p>
                <p>Dalam permainan battle royale, perebutan senjata di awal sangatlah penting. Sering kali terjadi perebutan senjata dengan pihak lawan. Berebut senjata di awal permainan membutuhkan skill pertarungan jarak dekat yang tidak mudah. Buat kalian yang belum terbiasa melakukan pertarungan jarak dekat. Antonio bisa dikatakan sebagai karakter yang memiliki darah yang cukup banyak karena kemampuan khususnya yaitu memiliki darah lebih banyak dari karakter lain. Hal ini cukup berguna untuk ketahanan dalam adu pukul dengan karakter lain karena Antonio punya darah yang lebih banyak dari yang lain.</p>
                <p><strong>3. Wolfrahh</strong></p>
                <p>Kemampuan aim dari pemain pemula belum sebagus pemain yang sudah lama bermain. Mengincar headshot pun cukup sulit untuk dilakukan kalau kemampuan aim pemainnya belum bagus. Karakter ini cocok untuk pemula karena kemampuannya menambah damage ketika tembakan mengenai tubuh. Karakter ini cukup bagus untuk dipakai pemula karena pemula yang belum terbiasa melakukan aim bisa membunuh dengan cepat tanpa harus mengincar headshot.</p>
                <p><strong>4. Ford</strong></p>
                <p>Terbunuh karena zona adalah hal yang sering dialami oleh pemain yang baru. Alasannya beragam, mulai dari belum hafal map sampai keasikan looting. Oleh karena itu kemampuan dari karakter ini cukup membantu. Kemampuan untuk menahan damage zona bisa membantu kalian para pemula yang sering mati karena termakan zona.</p>
                <p><strong>5. DJ Alok</strong></p>
                <p>Karakter DJ Alok ini bukan hanya diminati oleh para pemula tetapi juga oleh pemain profesional. Membeli DJ Alok terasa tidak akan rugi karena karakter ini bisa tetap dipakai meskipun pemain sudah bukan pemain pemula. Alasan karakter ini begitu digemari adalah kemampuannya yang bisa memberikan efek heal dan juga menambah kemampuan berlari. Karakter ini bisa bertahan lebih lama berkat kemampuannya yang sangat berguna itu.</p>
                <p>Itu adalah game recommendation hari ini, kira-kira karakter apa yang cocok dengan kalian? Selamat bermain, salam Booyah!</p>
                ',
                'content:id' => '
                <p>Free Fire menjadi game yang cukup populer di kalangan gamers saat ini. Game besutan Garena ini sering kali dimainkan oleh para youtuber gaming seperti Dyland Pros, Cepcill, dan juga Jess no Limit. Tidak heran banyak orang yang ingin mencoba memainkan permainan ini.</p>
                <p>Berbeda dengan game battle royale lainnya, Free Fire memiliki karakter dengan kemampuan yang berbeda-beda. Kemampuan yang dimiliki oleh setiap karakter bisa digunakan untuk membantu player yang baru belajar. Kira-kira siapa saja karakter yang cocok untuk digunakan pemula?</p>
                <p><strong>1. Kelly</strong></p>
                <p>Bisa bergerak dengan cepat merupakan kemampuan yang dimiliki oleh karakter ini. Semakin lama Kelly berlari akan semakin besar juga damage yang bisa dikeluarkannya. Kecepatan lari Kelly bisa membantu kalian para player untuk menjauh maupun mendekati musuh dengan mudah. Kalian juga bisa berpindah-pindah dengan cepat sehingga itu bisa membantu kalian menjelajahi map. Kemampuan ini sangat cocok untuk pemula yang masih ingin menghafal map.</p>
                <p><strong>2. Antonio</strong></p>
                <p>Dalam permainan battle royale, perebutan senjata di awal sangatlah penting. Sering kali terjadi perebutan senjata dengan pihak lawan. Berebut senjata di awal permainan membutuhkan skill pertarungan jarak dekat yang tidak mudah. Buat kalian yang belum terbiasa melakukan pertarungan jarak dekat. Antonio bisa dikatakan sebagai karakter yang memiliki darah yang cukup banyak karena kemampuan khususnya yaitu memiliki darah lebih banyak dari karakter lain. Hal ini cukup berguna untuk ketahanan dalam adu pukul dengan karakter lain karena Antonio punya darah yang lebih banyak dari yang lain.</p>
                <p><strong>3. Wolfrahh</strong></p>
                <p>Kemampuan aim dari pemain pemula belum sebagus pemain yang sudah lama bermain. Mengincar headshot pun cukup sulit untuk dilakukan kalau kemampuan aim pemainnya belum bagus. Karakter ini cocok untuk pemula karena kemampuannya menambah damage ketika tembakan mengenai tubuh. Karakter ini cukup bagus untuk dipakai pemula karena pemula yang belum terbiasa melakukan aim bisa membunuh dengan cepat tanpa harus mengincar headshot.</p>
                <p><strong>4. Ford</strong></p>
                <p>Terbunuh karena zona adalah hal yang sering dialami oleh pemain yang baru. Alasannya beragam, mulai dari belum hafal map sampai keasikan looting. Oleh karena itu kemampuan dari karakter ini cukup membantu. Kemampuan untuk menahan damage zona bisa membantu kalian para pemula yang sering mati karena termakan zona.</p>
                <p><strong>5. DJ Alok</strong></p>
                <p>Karakter DJ Alok ini bukan hanya diminati oleh para pemula tetapi juga oleh pemain profesional. Membeli DJ Alok terasa tidak akan rugi karena karakter ini bisa tetap dipakai meskipun pemain sudah bukan pemain pemula. Alasan karakter ini begitu digemari adalah kemampuannya yang bisa memberikan efek heal dan juga menambah kemampuan berlari. Karakter ini bisa bertahan lebih lama berkat kemampuannya yang sangat berguna itu.</p>
                <p>Itu adalah game recommendation hari ini, kira-kira karakter apa yang cocok dengan kalian? Selamat bermain, salam Booyah!</p>
                ',
                'slug:en' => '5-karakter-free-fire-yang-cocok-buat-pemula-mudah-dimainkan-en',
                'slug:id' => '5-karakter-free-fire-yang-cocok-buat-pemula-mudah-dimainkan-id',
            ],
        ];

        foreach ($article as $item) {
            Article::create($item);
        }
    }
}

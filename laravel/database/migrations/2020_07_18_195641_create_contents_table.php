<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->enum('section', [
                'global',
                'home',
                'teams',
                'match',
                'news',
                'partner',
                'contact'
            ]);
            $table->string('label');
            $table->string('hint')->nullable();
            $table->string('key')->unique();
            $table->string('validation')->default('required');
            $table->enum('type', ['text', 'file', 'wysiwyg', 'multiple_text']);
            $table->timestamps();
        });

        Schema::create('content_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('content_id')->unsigned();
            $table->string('locale')->index();

            $table->longText('value');

            $table->unique(['content_id', 'locale']);
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_translations');
        Schema::dropIfExists('contents');
    }
}
